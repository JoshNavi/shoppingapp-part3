-- Use as row headers
DROP TABLE IF EXISTS state_orders CASCADE;

CREATE TABLE state_orders(
	id          SERIAL PRIMARY KEY,
	state       TEXT NOT NULL,
  category_id INTEGER REFERENCES categories (id) NOT NULL,
	sum 	      FLOAT NOT NULL
);

INSERT INTO state_orders(state, category_id, sum)
(SELECT st.name, p.category_id AS category_id, COALESCE(SUM(s.price*s.quantity), 0) AS sum
FROM states st
LEFT OUTER JOIN users u ON u.state=st.name
LEFT OUTER JOIN orders s ON u.id = s.user_id
INNER JOIN products p ON p.id = s.product_id
GROUP BY st.name, category_id);

-- Used as colum headers
DROP TABLE IF EXISTS total_orders CASCADE;

CREATE TABLE total_orders(
  id          SERIAL PRIMARY KEY,
  product     TEXT NOT NULL,
  product_id INTEGER REFERENCES products (id) NOT NULL,
  category_id INTEGER REFERENCES categories (id) NOT NULL,
  sum 	      FLOAT NOT NULL
);

INSERT INTO total_orders(product, product_id, category_id, sum)
(SELECT p.name AS name, p.id AS product_id, p.category_id AS category_id, COALESCE(SUM(s.price*s.quantity), 0) AS sum
FROM products p
LEFT OUTER JOIN orders s ON p.id=s.product_id
GROUP BY p.id, p.name, p.category_id
ORDER BY sum DESC);

-- Grid
DROP TABLE IF EXISTS analytics CASCADE;

CREATE TABLE analytics(
  id          SERIAL PRIMARY KEY,
  state       TEXT NOT NULL,
  product     TEXT NOT NULL,
  sum 	      FLOAT NOT NULL
);

INSERT INTO analytics(state, product, sum)
(SELECT st.name, p.name, COALESCE(SUM(s.price*s.quantity), 0) AS sum
FROM states st
CROSS JOIN products p
LEFT OUTER JOIN users u ON u.state=st.name
LEFT OUTER JOIN orders s ON s.product_id=p.id AND s.user_id=u.id
GROUP BY st.name, p.name
ORDER BY st.name);

CREATE INDEX idx_anal_prod ON analytics(product);
CREATE INDEX idx_anal_state ON analytics(state);

-- Update state_orders
UPDATE state_orders s
SET sum = (s.sum + l.sum)
FROM (SELECT st.name AS state, p.category_id AS category_id, COALESCE(SUM(s.price*s.quantity), 0) AS sum
FROM states st, users u, logs s, products p
WHERE u.state=st.name
AND u.id = s.user_id
AND p.id = s.product_id
GROUP BY st.name, category_id) l
WHERE s.state = l.state AND s.category_id = l.category_id;

-- Update total_orders
UPDATE total_orders s
SET sum = (s.sum + l.sum)
FROM (SELECT p.name AS name, p.id AS product_id, p.category_id AS category_id, COALESCE(SUM(s.price*s.quantity), 0) AS sum
FROM products p, logs s
WHERE p.id=s.product_id
GROUP BY p.id, p.name, p.category_id
ORDER BY sum DESC) l
WHERE s.product_id = l.product_id AND s.category_id = l.category_id;

-- Update analytics
UPDATE analytics s
SET sum = (s.sum + l.sum)
FROM (SELECT st.name AS state, p.name AS product, COALESCE(SUM(s.price*s.quantity), 0) AS sum
FROM logs s, states st, products p, users u
WHERE u.state=st.name
AND s.product_id=p.id
AND s.user_id=u.id
GROUP BY st.name, p.name
ORDER BY st.name) l
WHERE s.state = l.state AND s.product = l.product;

DELETE FROM logs *;
