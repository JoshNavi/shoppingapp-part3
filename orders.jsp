<%!
public static String selected (String name, String value) {
  String val = "value=\""+value+"\"";
  if (value.equals(name)) {
    return val+" selected";
  }
  else {
    return val;
  }
}
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.sql.*, javax.sql.*, javax.naming.*, java.util.*, java.text.DecimalFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<title>CSE135 Project</title>
</head>
<%
  Connection conn = null;
  Statement stmt = null;
  ResultSet rs = null;
	try {
		Class.forName("org.postgresql.Driver");
    String url = "jdbc:postgresql:cse135part2";
  	String admin = "postgres";
  	String password = "postgres";
  	conn = DriverManager.getConnection(url, admin, password);
	}
	catch (Exception e) {}

  stmt = conn.createStatement();

	if ("POST".equalsIgnoreCase(request.getMethod())) {
		String action = request.getParameter("submit");
		if (action.equals("insert")) {
			int queries_num = Integer.parseInt(request.getParameter("queries_num"));
			Random rand = new Random();
			int random_num = rand.nextInt(30) + 1;
			if (queries_num < random_num) random_num = queries_num;

      // Add orders to log
      rs = stmt.executeQuery("SELECT MAX(id) AS max FROM orders");
      int oldMax = 0;
      if(rs.next()) {
        oldMax= rs.getInt("max");
      }
			stmt.executeQuery("SELECT proc_insert_orders(" + queries_num + "," + random_num + ")");
      stmt.executeUpdate("INSERT INTO logs (order_id, user_id, product_id, quantity, price, is_cart) (SELECT * FROM orders WHERE id >" + oldMax + " )");
			out.println("<script>alert('" + queries_num + " orders are inserted!');</script>");
		}
		else if (action.equals("refresh")) {
			//Need to implement.
		}
	}
%>

<%
if ("GET".equalsIgnoreCase(request.getMethod())) {
  // Run queries to modify precomputed values based on logs
  rs = stmt.executeQuery("SELECT COUNT(*) as cnt FROM logs");
  if(rs.next()) {
    if(rs.getInt("cnt") > 0) {
      System.out.println("Need to update precomputed values");

      // Update rows
      stmt.executeUpdate("UPDATE state_orders s"
        + " SET sum = (s.sum + l.sum)"
        + " FROM (SELECT st.name AS state, p.category_id AS category_id, COALESCE(SUM(s.price*s.quantity), 0) AS sum"
        + " FROM states st, users u, logs s, products p"
        + " WHERE u.state=st.name"
        + " AND u.id = s.user_id"
        + " AND p.id = s.product_id"
        + " GROUP BY st.name, category_id) l"
        + " WHERE s.state = l.state AND s.category_id = l.category_id");

      // Update cols
      stmt.executeUpdate("UPDATE total_orders s"
        + " SET sum = (s.sum + l.sum)"
        + " FROM (SELECT p.name AS name, p.id AS product_id, p.category_id AS category_id, COALESCE(SUM(s.price*s.quantity), 0) AS sum"
        + " FROM products p, logs s"
        + " WHERE p.id=s.product_id"
        + " GROUP BY p.id, p.name, p.category_id"
        + " ORDER BY sum DESC) l"
        + " WHERE s.product_id = l.product_id AND s.category_id = l.category_id");

      // Update grid
      stmt.executeUpdate("UPDATE analytics s"
        + " SET sum = (s.sum + l.sum)"
        + " FROM (SELECT st.name AS state, p.name AS product, COALESCE(SUM(s.price*s.quantity), 0) AS sum"
        + " FROM logs s, states st, products p, users u"
        + " WHERE u.state=st.name"
        + " AND s.product_id=p.id"
        + " AND s.user_id=u.id"
        + " GROUP BY st.name, p.name"
        + " ORDER BY st.name) l"
        + " WHERE s.state = l.state AND s.product = l.product");

      stmt.executeUpdate("DELETE FROM logs *");
    }
  }
}

%>

<%
final int MAX_COLS = 50;
String cid;
String catFilter;
String prod_table = "temp_prod";
String order_table = "temp_orders";
String prodInfoQuery, rowInfoQuery, ordersInfoQuery;
DecimalFormat df = new DecimalFormat("#.00");

if (request.getParameter("cid") != null && !request.getParameter("cid").equals("all")) {
  cid = request.getParameter("cid");
  catFilter = "p.category_id = " + cid;
}
else {
  cid = "all";
  catFilter = "p.category_id > 0";
}
%>

<body>
<div class="collapse navbar-collapse">
	<ul class="nav navbar-nav">
		<li><a href="index.jsp">Home</a></li>
		<li><a href="categories.jsp">Categories</a></li>
		<li><a href="products.jsp">Products</a></li>
		<li><a href="orders.jsp">Orders</a></li>
		<li><a href="login.jsp">Logout</a></li>
	</ul>
</div>
<div>
<form action="orders.jsp" method="GET">
  <table class="table table-striped">
  <tr>
    <th>Category</th>
    <th>&nbsp;</th>
  </tr>
  <tr>
    <td>
      <select name="cid" id="cid" class="form-control">
        <option value="all">All categories</option>
        <%
        rs = stmt.executeQuery("SELECT * FROM categories");
        while(rs.next()) {
        %>
          <option <%=selected(cid, rs.getString("id")) %> > <%=rs.getString("name")%> </option>
        <%
        }
        %>
      </select>
    </td>
    <td>
      <input class="btn btn-primary btn-block" type="submit" value="Run" />
    </td>
  </tr>
  </table>
</form>

<div><h1 id="message"> </h1></div>

<form action="orders.jsp" method="POST">
	<label># of queries to insert</label>
	<input type="number" name="queries_num">
	<input class="btn btn-primary"  type="submit" name="submit" value="insert"/>
</form>

<form action="orders.jsp" method="POST" style="display:inline-block;">
	<input class="btn btn-success"  type="submit" name="submit" value="refresh" onclick="refreshPage(event)"/>
</form>

<form action="orders.jsp" method="POST" style="display:inline-block;float:right;">
	<input class="btn btn-success"  type="submit" name="submit" value="refresh" onclick="refreshPage(event)"/>
</form>

<table class="table table-striped">
  <tr>
    <th>States</th>

    <%
    // rs = stmt.executeQuery(prodInfoQuery);
    rs = stmt.executeQuery("SELECT *"
      + " FROM total_orders p"
      + " WHERE " + catFilter
      + " ORDER BY sum DESC"
      + " LIMIT 50"
    );

    int cols = 0;
    String[] products = new String[MAX_COLS+1];

    while(rs.next()) {
      products[cols] = rs.getString("product");
      cols++;
      %>
      <th id="<%=rs.getString("product")%>" class="<%=rs.getString("product")%>" data-sum="<%=rs.getString("sum")%>" >
        <%=rs.getString("product")%>
        <br>
          ($<%=df.format(Double.parseDouble(rs.getString("sum")))%>)
        </th>
      <%
    }
    %>
    <td>
      <input class="btn btn-success"  type="submit" name="submit" value="refresh" onclick="refreshPage(event)"/>
    </td>
  </tr>

  <%
  rs = stmt.executeQuery("SELECT state, SUM(sum)"
    + " FROM state_orders p"
    + " WHERE " + catFilter
    + " GROUP BY state"
    + " ORDER BY sum DESC"
  );

  PreparedStatement pstmt = conn.prepareStatement(
    "SELECT sum"
    + " FROM analytics"
    + " WHERE product = ?"
    + " AND state = ?"
  );
  ResultSet infoRS = null;

  while(rs.next()) {
    pstmt.setString(2, rs.getString("state"));
    %>
    <tr>
      <th id="<%=rs.getString("state")%>" data-sum="<%=rs.getString("sum")%>">
        <%=rs.getString("state")%> <br> ($<%=df.format(Double.parseDouble(rs.getString("sum")))%>)
      </th>
    <%
    int i;
    for(i = 0; i < cols; i++) {
      pstmt.setString(1, products[i]);
      infoRS = pstmt.executeQuery();
      if(infoRS.next()) {
        %>
        <td id="<%=rs.getString("state") + "," + products[i]%>" class="<%=products[i]%>" data-sum="<%=rs.getString("sum")%>">
          $<%=df.format(Double.parseDouble(infoRS.getString("sum")))%>
        </td>
        <%
      }
    }
    %>
    <td>
    </td>
  </tr>
  <%
  }

  infoRS.close();
  pstmt.close();
  %>
</table>

<form action="orders.jsp" method="POST" style="display:inline-block;">
	<input class="btn btn-success"  type="submit" name="submit" value="refresh" onclick="refreshPage(event)"/>
</form>

<form action="orders.jsp" method="POST" style="display:inline-block;float:right;">
	<input class="btn btn-success"  type="submit" name="submit" value="refresh" onclick="refreshPage(event)"/>
</form>

<script>
function refreshPage(e) {
  e.preventDefault(); // Dont refresh

  var xmlHttp;
  try {
   xmlHttp = new XMLHttpRequest(); // Firefox, Opera, Safari
  }
  catch (e) {
    try {
     xmlHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
    }
    catch (e) {
      try {
        xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
      }
      catch (e) {
        alert("Your browser does not support Ajax! Please click Run");
        return false;
      }
    }
  }

  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState==4 && xmlHttp.status == 200) {
      var x = xmlHttp.responseText;
      var changes = JSON.parse(x);
      console.log(changes);

      for(var i = 0; i< changes.states.length; i++){
        var elem = document.getElementById(changes.states[i].state);
        var data = parseFloat(elem.getAttribute("data-sum")) + parseFloat(changes.states[i].info);
        elem.setAttribute("data-sum", data)
        elem.style.color =  "red";
        elem.innerHTML =  changes.states[i].state + "<br> ($" + data.toFixed(2) + ")";
      }

      for(var i = 0; i < changes.products.length; i++){
        var elem = document.getElementById(changes.products[i].product);

        if(elem) {
          var data = parseFloat(elem.getAttribute("data-sum")) + parseFloat(changes.products[i].info);
          elem.setAttribute("data-sum", data)
          elem.style.color = "red";
          elem.innerHTML =  changes.products[i].product + "<br> ($" + data.toFixed(2) + ")";
        }
      }

      for(var i = 0; i < changes.grid.length; i++){
        var elem = document.getElementById(changes.grid[i].state+","+changes.grid[i].product);
        if(elem) {
          var data = parseFloat(elem.getAttribute("data-sum")) + parseFloat(changes.grid[i].info);
          elem.setAttribute("data-sum", data)
          elem.style.color = "red";
          elem.innerHTML = "$" + data.toFixed(2);
        }
      }

      for(var i = 0; i < changes.purple.length; i++){
        var y = document.getElementsByClassName(changes.purple[i].product);
        if(y) {
          for(var j = 0; j< y.length; j++){
            y[j].style.backgroundColor = "purple";
          }
        }
      }

      var message = "New products in the top 50 (not shown): ";

      for(var i = 0; i < changes.new.length; i++){
        message += changes.new[i].product + ": $" + changes.new[i].sum + ", ";
      }

      document.getElementById("message").innerHTML = message;
    }
  };

  xmlHttp.open("GET","refresh.jsp",true);
  xmlHttp.send(null);
}

</script>
</body>
</html>
