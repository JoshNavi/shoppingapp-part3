<%@ page contentType="text/html" import="java.sql.*, javax.sql.*, javax.naming.*" %>
<%@ page import="java.util.*, org.json.JSONObject, org.json.JSONArray"%>

<%
Connection conn = null;
Statement stmt = null;
ResultSet rs = null;
ResultSet colrs = null;
ResultSet rowrs = null;
ResultSet gridrs = null;

try {
	Class.forName("org.postgresql.Driver");
  String url = "jdbc:postgresql:cse135part2";
	String admin = "postgres";
	String password = "postgres";
	conn = DriverManager.getConnection(url, admin, password);
}
catch (Exception e) {}

stmt = conn.createStatement();

JSONObject changes = new JSONObject();

JSONArray redProds = new JSONArray();
JSONArray redStates = new JSONArray();
JSONArray redGrid = new JSONArray();
JSONArray purpleCols = new JSONArray();
JSONArray newTopProds = new JSONArray();

// Run queries to modify precomputed values based on logs
rs = stmt.executeQuery("SELECT COUNT(*) as cnt FROM logs");
if(rs.next()) {
  if(rs.getInt("cnt") > 0) {
    System.out.println("Need to update precomputed values");

    // Update rows
    rowrs = stmt.executeQuery("SELECT st.name AS state, COALESCE(SUM(s.price*s.quantity), 0) AS sum"
      + " FROM states st, users u, logs s, products p"
      + " WHERE u.state=st.name"
      + " AND u.id = s.user_id"
      + " AND p.id = s.product_id"
      + " GROUP BY st.name"
      + " ORDER BY sum DESC");

		while(rowrs.next()) {
		// String product = turnRed.getString("product_id");
			JSONObject redStatesNode = new JSONObject();
			String state = rowrs.getString("state");
			String sum = rowrs.getString("sum");
			String info = sum;
			redStatesNode.put("state", state);
			redStatesNode.put("info", info);
			redStates.put(redStatesNode);
		}


    // Update columns
    colrs = stmt.executeQuery("SELECT p.name AS name, p.id AS product_id, p.category_id AS category_id, COALESCE(SUM(s.price*s.quantity), 0) AS sum"
      + " FROM products p, logs s"
      + " WHERE p.id=s.product_id"
      + " GROUP BY p.id, p.name, p.category_id"
      + " ORDER BY sum DESC");

		while(colrs.next()) {
			JSONObject redProdsNode = new JSONObject();
      String product = colrs.getString("name");
      String sum =colrs.getString("sum");
      String info = sum;
      redProdsNode.put("product", product);
      redProdsNode.put("info", info);
      redProds.put(redProdsNode);
    }

    // Update grid
    gridrs = stmt.executeQuery("SELECT st.name AS state, p.name AS product, COALESCE(SUM(s.price*s.quantity), 0) AS sum"
      + " FROM logs s, states st, products p, users u"
      + " WHERE u.state=st.name"
      + " AND s.product_id=p.id"
      + " AND s.user_id=u.id"
      + " GROUP BY st.name, p.name"
      + " ORDER BY st.name");

    // stmt.executeUpdate("DELETE FROM logs *");

    while(gridrs.next()) {
			JSONObject redGridNode = new JSONObject();
      String product = gridrs.getString("product");
      String state = gridrs.getString("state");
      String sum = gridrs.getString("sum");
      String info = sum;
      redGridNode.put("product", product);
      redGridNode.put("state", state);
      redGridNode.put("info", info);
      redGrid.put(redGridNode);
    }

		ArrayList<String> oldTop50 = new ArrayList<String>();
		ArrayList<String> newTop50 = new ArrayList<String>();
		ArrayList<String> newTop50Sum = new ArrayList<String>();

		ResultSet toprs = stmt.executeQuery("SELECT *"
			+ " FROM total_orders p"
			+ " ORDER BY sum DESC"
			+ " LIMIT 50"
		);

		while(toprs.next()) {
			oldTop50.add(toprs.getString("product"));
		}

		// Update rows
		stmt.executeUpdate("UPDATE state_orders s"
			+ " SET sum = (s.sum + l.sum)"
			+ " FROM (SELECT st.name AS state, p.category_id AS category_id, COALESCE(SUM(s.price*s.quantity), 0) AS sum"
			+ " FROM states st, users u, logs s, products p"
			+ " WHERE u.state=st.name"
			+ " AND u.id = s.user_id"
			+ " AND p.id = s.product_id"
			+ " GROUP BY st.name, category_id) l"
			+ " WHERE s.state = l.state AND s.category_id = l.category_id");

		// Update cols
		stmt.executeUpdate("UPDATE total_orders s"
			+ " SET sum = (s.sum + l.sum)"
			+ " FROM (SELECT p.name AS name, p.id AS product_id, p.category_id AS category_id, COALESCE(SUM(s.price*s.quantity), 0) AS sum"
			+ " FROM products p, logs s"
			+ " WHERE p.id=s.product_id"
			+ " GROUP BY p.id, p.name, p.category_id"
			+ " ORDER BY sum DESC) l"
			+ " WHERE s.product_id = l.product_id AND s.category_id = l.category_id");

		// Update grid
		stmt.executeUpdate("UPDATE analytics s"
			+ " SET sum = (s.sum + l.sum)"
			+ " FROM (SELECT st.name AS state, p.name AS product, COALESCE(SUM(s.price*s.quantity), 0) AS sum"
			+ " FROM logs s, states st, products p, users u"
			+ " WHERE u.state=st.name"
			+ " AND s.product_id=p.id"
			+ " AND s.user_id=u.id"
			+ " GROUP BY st.name, p.name"
			+ " ORDER BY st.name) l"
			+ " WHERE s.state = l.state AND s.product = l.product");

		// Delete old logs
		stmt.executeUpdate("DELETE FROM logs *");

		toprs = stmt.executeQuery("SELECT *"
			+ " FROM total_orders p"
			+ " ORDER BY sum DESC"
			+ " LIMIT 50"
		);

		while(toprs.next()) {
			newTop50.add(toprs.getString("product"));
			newTop50Sum.add(toprs.getString("sum"));
		}

		for(int i = 0; i < oldTop50.size(); i++) {
			if( !newTop50.contains(oldTop50.get(i)) ) {
				JSONObject purpleNode = new JSONObject();
				purpleNode.put("product", oldTop50.get(i));
				purpleCols.put(purpleNode);
			}

			if( !oldTop50.contains(newTop50.get(i)) ) {
				JSONObject newProdsNode = new JSONObject();
				newProdsNode.put("product", newTop50.get(i));
				newProdsNode.put("sum", newTop50Sum.get(i));
				newTopProds.put(newProdsNode);
			}

		}

    changes.put("states", redStates);
    changes.put("products", redProds);
    changes.put("grid", redGrid);
		changes.put("purple", purpleCols);
		changes.put("new", newTopProds);
  }

  out.print(changes);
}
%>
